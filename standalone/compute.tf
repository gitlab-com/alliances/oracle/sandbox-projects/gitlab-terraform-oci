resource "oci_core_instance" "simple-vm" {
  availability_domain = local.availability_domain
  compartment_id      = var.compute_compartment_ocid
  display_name        = var.vm_display_name
  shape               = local.compute_shape

  dynamic "shape_config" {
    for_each = local.is_flex_shape
    content {
      ocpus         = local.flex_shape_ocpus
      memory_in_gbs = local.flex_shape_memory
    }
  }


  create_vnic_details {
    subnet_id              = local.use_existing_network ? var.subnet_id : oci_core_subnet.simple_subnet[0].id
    display_name           = var.subnet_display_name
    assign_public_ip       = local.is_public_subnet
    hostname_label         = local.use_hostnames ? var.hostname_label : null
    skip_source_dest_check = false
    nsg_ids                = [oci_core_network_security_group.simple_nsg.id]
  }

  source_details {
    source_type = "image"
    source_id   = local.image_id
  }


  // #"./scripts/setup_omnibox.sh"
  // #(local.platform_image_os == "ubuntu" ? file(data.template_file.setup_gitlab.rendered) : ""),
  //   metadata = {
  //     ssh_authorized_keys = var.ssh_public_key
  //     user_data = base64encode(join("\n", list(
  //       "#!/usr/bin/env bash",
  //       "set -x",
  //       (local.is_ubuntu ? data.template_file.setup_gitlab-ubuntu.rendered : ""),
  //     )))
  //   }


  metadata = {
    ssh_authorized_keys = var.ssh_public_key
    user_data = base64encode(join("\n", list(
      "#!/usr/bin/env bash",
      "set -x",
      (var.external_url != "" ? "export IS_EXTERNAL_URL=1" : "export IS_EXTERNAL_URL=0"),
      (var.external_url != "" ? "export EXTERNAL_URL=\"${var.external_url}\"" : ""),
      #      "export initial_passwd=\"${random_password.password.result}\"",
      data.template_file.setup_gitlab-OL.rendered,
    )))
  }

  freeform_tags = map(var.tag_key_name, var.tag_value)



}

### Oracle Linux 8
data "template_file" "setup_gitlab-OL" {
  template = file("${path.module}/scripts/setup_omnibox-OL8.sh")
}