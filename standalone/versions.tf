terraform {
  required_version = "~> 0.14"
  required_providers {
    oci = {
      version = ">= 3.27"
    }
  }
}