# gitlab-terraform-oci\gitlab-runner

The [Oracle Cloud Infrastructure (OCI) Quick Start](https://github.com/oracle-quickstart?q=oci-quickstart) is a collection of examples that allow Oracle Cloud Infrastructure users to get a quick start deploying advanced infrastructure on OCI.

gitlab-runner contains the Terraform template that can be used for deploying GitLab Runner that runs from local Terraform CLI, [OCI Resource Manager](https://docs.cloud.oracle.com/en-us/iaas/Content/ResourceManager/Concepts/resourcemanager.htm) and [OCI Cloud Shell](https://docs.cloud.oracle.com/en-us/iaas/Content/API/Concepts/cloudshellintro.htm).

gitlab-terraform-oci\gitlab-runner Terraform template deploys Compute Instances with GitLab Runner installed on top of an existing or brand new Virtual Cloud Network. This Runner is setup as a *Docker* executor.

This repo is under active development.  Building open source software is a community effort.  We're excited to engage with the community building this.

## Prerequisites

First off we'll need to do some pre deploy setup.  That's all detailed [here](https://github.com/oracle/oci-quickstart-prerequisites).

## Resource Manager Deployment

This Quick Start uses [OCI Resource Manager](https://docs.cloud.oracle.com/iaas/Content/ResourceManager/Concepts/resourcemanager.htm) to make deployment quite easy. Simply `build` your package and follow the [Resource Manager instructions](https://docs.cloud.oracle.com/en-us/iaas/Content/ResourceManager/Tasks/managingstacksandjobs.htm#console) for how to create a stack.  Prior to building the Stack, you may want to modify some parts of the deployment detailed in the sections below.
Alternatively, you can click on the *Deploy to Oracle Cloud* button to automatically create your ORM Stack from the latest published file.

In case you want to build the Stack, make sure you have terraform v0.14+ cli installed and accessible from your terminal.

```bash
terraform -v

Terraform v0.14.9
+ provider registry.terraform.io/hashicorp/oci v4.21.0
+ provider registry.terraform.io/hashicorp/template v2.2.0
```

### Build

In order to `build` the zip file with the latest changes you made to this code, you can simply go to [build-orm](./build-orm) folder and use terraform to generate a new zip file:

At first time, you are required to initialize the terraform modules used by the template with  `terraform init` command:

```bash
$ Initializing the backend...

Initializing provider plugins...
- Reusing previous version of hashicorp/archive from the dependency lock file
- Installing hashicorp/archive v2.1.0...
- Installed hashicorp/archive v2.1.0 (signed by HashiCorp)

Terraform has been successfully initialized!
```

Once terraform is initialized, just run `terraform apply` to generate ORM zip file.

```bash
$ terraform apply

data.archive_file.generate_zip: Refreshing state...

Apply complete! Resources: 0 added, 0 changed, 0 destroyed.
```

This command will package the content of `simple` folder into a zip and will store it in the `build-orm\dist` folder. You can check the content of the file by running `unzip -l dist/oci-gitlab-runner-orm.zip`:

```bash
$ unzip -l dist/oci-gitlab-runner-orm.zip       
Archive:  dist/oci-gitlab-runner-orm.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
     1933  01-01-2049 00:00   .terraform.lock.hcl
     1796  01-01-2049 00:00   compute.tf
     1372  01-01-2049 00:00   data_sources.tf
     1376  01-01-2049 00:00   locals.tf
     2000  01-01-2049 00:00   network.tf
     2480  01-01-2049 00:00   nsg.tf
    17334  01-01-2049 00:00   orm.yaml
      657  01-01-2049 00:00   outputs.tf
     1861  01-01-2049 00:00   scripts/install-gitlab-runner-docker-ol79.sh
     5860  01-01-2049 00:00   variables.tf
       46  01-01-2049 00:00   versions.tf
---------                     -------
    36715                     11 files
```

### Deploy

You can deploy the latest build available in the GitLab repository by simply clicking on [![Deploy to Oracle Cloud](https://oci-resourcemanager-plugin.plugins.oci.oraclecloud.com/latest/deploy-to-oracle-cloud.svg)](https://console.us-phoenix-1.oraclecloud.com/resourcemanager/stacks/create?region=home&zipUrl=https://gitlab.com/gitlab-com/alliances/oracle/sandbox-projects/gitlab-terraform-oci/-/raw/master/build-orm/dist/oci-gitlab-orm.zip) or deploy a local copy you previously generated based on the instructions below.

1. Click on [Login](https://console.us-ashburn-1.oraclecloud.com/resourcemanager/stacks/create) to Oracle Cloud Infrastructure to import the stack
    > `Home > Solutions & Platform > Resource Manager > Stacks > Create Stack`

2. Upload the `oci-gitlab-runner-orm.zip` and provide a name and description for the stack
![Create Stack](./images/create_orm_stack.png)

3. Configure the Stack. The UI will present the variables to the user dynamically, based on their selections. These are the configuration options:

> GitLab Runner Configuration (Docker executor)

|          VARIABLE          |           DESCRIPTION                                                 |
|----------------------------|-----------------------------------------------------------------------|
|GITLAB SERVER URL           | The URL of your GitLab Server. E.g. http://gitlab.example.com                                         |
|REGISTRATION TOKEN          | The Token value available on the Runners section of your GitLab server|
|NUMBER OF RUNNER INSTANCES  | The total number of Compute instances to deploy                       |
|BASE DOCKER IMAGE           | Base Docker image setup on the Runner for running CI/CD pipelines     |
|RUNNER TAG LIST             | Tags associated with the Runner                                       |

> Compute Configuration

|          VARIABLE          |           DESCRIPTION                                                 |
|----------------------------|-----------------------------------------------------------------------|
|COMPUTE COMPARTMENT         | Compartment where GitLab Runner Compute resources will be deployed |
|AVAILABILITY DOMAIN         | Availability Domain|
|INSTANCE NAME               | Compute instance name.|
|DNS HOSTNAME LABEL          | DNS Hostname Label|
|COMPUTE SHAPE               | Compute shape|
|FLEX SHAPE MEMORY           | Amount of Memory in GB. Only available for *Flex* Compute shapes. Minimum/Maximum memory amount depends on shape and OCPUs|
|FLEX SHAPE OCPUS            | Number of OCPUs, only available for *Flex* Compute shape|
|OPERATING SYSTEM            | Compute instance Operating System (supported by the Template)|
|COMPUTE IMAGE STRATEGY      | Use either a `Platform Image` or `Custom Image`|
|PLATFORM IMAGE*             | When `PLATFORM IMAGE` is chosen, select a Platform Image from the list |
|CUSTOM IMAGE*               | When `CUSTOM IMAGE` is chosen, enter the Custom Image OCID (must be compatible with selected Operating System) |
|PUBLIC SSH KEY              | RSA PUBLIC SSH key used for SSH to the OS|

> Virtual Cloud Network

|          VARIABLE          |           DESCRIPTION                                                 |
|----------------------------|-----------------------------------------------------------------------|
|NETWORK COMPARTMENT         | Compartment for all Virtual Cloud Nettwork resources|
|NETWORK STRATEGY            | `Create New VCN and Subnet`: Create new network resources during apply. <br> `Use Existing VCN and Subnet`: Let user select pre-existent network resources.|
|CONFIGURATION STRATEGY      | `Use Recommended Configuration`: Use default configuration defined by the Terraform template. <br> `Customize Network Configuration`: Allow user to customize some network configuration such as name, dns label, cidr block for VCN and Subnet.|

> Virtual Cloud Network - Customize Network Configuration

|          VARIABLE          |           DESCRIPTION                                                 |
|----------------------------|-----------------------------------------------------------------------|
|NAME                        | VCN Display Name|
|DNS LABEL                   | VCN DNS LABEL|
|CIDR BLOCK                  | The CIDR of the new Virtual Cloud Network (VCN). If you plan to peer this VCN with another VCN, the VCNs must not have overlapping CIDRs.|

> Subnet (visible when `Customize Network Configuration` is selected or `Use Existing VCN and Subnet`)

|          VARIABLE          |           DESCRIPTION                                                 |
|----------------------------|-----------------------------------------------------------------------|
|SUBNET TYPE                 | `Public Subnet` or `Private Subnet`|
|EXISTING SUBNET*            | Visible when `Use Existing VCN and Subnet` selected. List of Subnets available on the selected VCN based on the Subnet Type|
|NAME                        | Subnet Display Name|
|DNS LABEL                   | Subnet DNS LABEL|
|CIDR BLOCK                  | The CIDR of the Subnet. Should not overlap with any other subnet CIDRs|
|NETWORK SECURITY GROUP CONFIGURATION| `Use Recommended Configuration`: Use default configuration defined by the Terraform template. <br> `Customize Network Security Group`: Allow user to customize some basic network security group settings.|

> Network Security Group (visible only when `Customize Network Security Group` is selected)

|          VARIABLE          |           DESCRIPTION                                                 |
|----------------------------|-----------------------------------------------------------------------|
|NAME                        | NSG Display Name|
|ALLOWED INGRESS TRAFFIC (CIDR BLOCK)| WHITELISTED CIDR BLOCK for ingress traffic|
|SSH PORT NUMBER             | Default SSH PORT for ingress traffic|
|HTTP PORT NUMBER            | Default HTTP PORT for ingress traffic|
|HTTPS PORT NUMBER           | Default HTTPS PORT for ingress traffic|

> Additional Configuration Options

|          VARIABLE          |           DESCRIPTION                                                 |
|----------------------------|-----------------------------------------------------------------------|
|TAG KEY NAME                | Free-form tag key name|
|TAG VALUE                   | Free-form tag value|

4. Click Next and Review the configuration.
5. Click Create button to confirm and create your ORM Stack.
6. On Stack Details page, you can now run `Terraform` commands to manage your infrastructure. You typically start with a `plan` then run `apply` to create and make changes to the infrastructure. More details below:

|      TERRAFORM ACTIONS     |           DESCRIPTION                                                 |
|----------------------------|-----------------------------------------------------------------------|
|Plan                        | `terraform plan` is used to create an execution plan. This command is a convenient way to check the execution plan prior to make any changes to the infrastructure resources.|
|Apply                       | `terraform apply` is used to apply the changes required to reach the desired state of the configuration described by the template.|
|Destroy                     | `terraform destroy` is used to destroy the Terraform-managed infrastructure.|
