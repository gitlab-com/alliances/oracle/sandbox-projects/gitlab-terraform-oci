resource "oci_core_vcn" "simple" {
  count          = local.use_existing_network ? 0 : 1
  cidr_block     = var.vcn_cidr_block
  dns_label      = substr(var.vcn_dns_label, 0, 15)
  compartment_id = var.network_compartment_ocid
  display_name   = var.vcn_display_name

  freeform_tags = map(var.tag_key_name, var.tag_value)
}

#IGW
resource "oci_core_internet_gateway" "simple_internet_gateway" {
  count          = (local.use_existing_network || !local.is_public_subnet) ? 0 : 1
  compartment_id = var.network_compartment_ocid
  vcn_id         = oci_core_vcn.simple[count.index].id
  enabled        = "true"
  display_name   = "${var.vcn_display_name}-igw"

  freeform_tags = map(var.tag_key_name, var.tag_value)
}

#NAT Gateway
resource "oci_core_nat_gateway" "simple_nat_gateway" {
  count          = (local.use_existing_network || local.is_public_subnet) ? 0 : 1
  compartment_id = var.network_compartment_ocid
  vcn_id         = oci_core_vcn.simple[0].id
  display_name   = "${var.vcn_display_name}-NATgw"

  freeform_tags = map(var.tag_key_name, var.tag_value)
}


#simple subnet
resource "oci_core_subnet" "simple_subnet" {
  count                      = local.use_existing_network ? 0 : 1
  cidr_block                 = var.subnet_cidr_block
  compartment_id             = var.network_compartment_ocid
  vcn_id                     = oci_core_vcn.simple[count.index].id
  display_name               = var.subnet_display_name
  dns_label                  = substr(var.subnet_dns_label, 0, 15)
  prohibit_public_ip_on_vnic = !local.is_public_subnet

  freeform_tags = map(var.tag_key_name, var.tag_value)
}

resource "oci_core_route_table" "public_route_table" {
  count          = (local.use_existing_network || !local.is_public_subnet) ? 0 : 1
  compartment_id = var.network_compartment_ocid
  vcn_id         = oci_core_vcn.simple[count.index].id
  display_name   = "${var.subnet_display_name}-public-rt"

  route_rules {
    network_entity_id = oci_core_internet_gateway.simple_internet_gateway[count.index].id
    destination       = "0.0.0.0/0"
    destination_type  = "CIDR_BLOCK"
  }

  freeform_tags = map(var.tag_key_name, var.tag_value)
}


resource "oci_core_route_table" "private_route_table" {
  count          = (local.use_existing_network || local.is_public_subnet) ? 0 : 1
  compartment_id = var.network_compartment_ocid
  vcn_id         = oci_core_vcn.simple[0].id
  display_name   = "${var.subnet_display_name}-private-rt"

  route_rules {
    network_entity_id = oci_core_nat_gateway.simple_nat_gateway[count.index].id
    destination       = "0.0.0.0/0"
    destination_type  = "CIDR_BLOCK"
  }

  freeform_tags = map(var.tag_key_name, var.tag_value)
}


resource "oci_core_route_table_attachment" "route_table_attachment" {
  count          = local.use_existing_network ? 0 : 1
  subnet_id      = oci_core_subnet.simple_subnet[count.index].id
  route_table_id = local.is_public_subnet ? oci_core_route_table.public_route_table[count.index].id : oci_core_route_table.private_route_table[count.index].id
}