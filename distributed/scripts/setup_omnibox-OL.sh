install_gitlab() {

    firewall-offline-cmd --add-service=http
    firewall-offline-cmd --add-service=https
    systemctl reload firewalld
    sleep 30
    curl -O https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh 
    os=el dist=8 bash script.rpm.sh    

    arch=$(uname -m)
    if [ "$arch" == "aarch64" ]; then
        dnf install -y gitlab-ee-13.8.6-ee.0.el8.aarch64
    else
        dnf install -y gitlab-ee
    fi
}

setup_gitlab_config() {

    firewall-offline-cmd --zone=public  --add-port=80/tcp
    firewall-offline-cmd --zone=public  --add-port=443/tcp
    systemctl restart firewalld.service

    sed -i"" "s#external_url 'http://gitlab.example.com'##g" /etc/gitlab/gitlab.rb
    echo "external_url '$external_url'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['initial_root_password'] = '$initial_passwd'" >> /etc/gitlab/gitlab.rb

    ## Nginx details
    echo "nginx['listen_port'] = 80" >> /etc/gitlab/gitlab.rb
    echo "nginx['listen_https'] = false" >> /etc/gitlab/gitlab.rb
 
    ## Gitaly and GitLab use two shared secrets for authentication
    echo "gitlab_rails['gitaly_token'] = '$initial_passwd'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_shell['secret_token'] = '$initial_passwd'" >> /etc/gitlab/gitlab.rb

    ## Gitaly details
    echo "git_data_dirs({" >> /etc/gitlab/gitlab.rb
    echo "  'default' => { 'gitaly_address' => 'tcp://$gitaly_server1_ip_env_var:8075' }," >> /etc/gitlab/gitlab.rb
    echo "  'storage1' => { 'gitaly_address' => 'tcp://$gitaly_server2_ip_env_var:8075' }," >> /etc/gitlab/gitlab.rb
    echo "})" >> /etc/gitlab/gitlab.rb

    ## Disable components that will not be on the GitLab application server
    echo "roles ['application_role']" >> /etc/gitlab/gitlab.rb
    echo "gitaly['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "nginx['enable'] = true" >> /etc/gitlab/gitlab.rb
    
    ## PostgreSQL connection details
    echo "gitlab_rails['db_adapter'] = 'postgresql'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['db_encoding'] = 'unicode'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['db_password'] = '$POSTGRESQL_PASSWD'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['db_host'] = '$postgres_server_ip_env_var'" >> /etc/gitlab/gitlab.rb

    ## Redis connection details
    echo "gitlab_rails['redis_host'] = '$redis_server_ip_env_var'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['redis_port'] = 6379" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['redis_password'] = '$initial_passwd'" >> /etc/gitlab/gitlab.rb
    
    ## Set the network addresses that the exporters used for monitoring 
    echo "node_exporter['listen_address'] = '0.0.0.0:9100'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_workhorse['prometheus_listen_addr'] = '0.0.0.0:9229'" >> /etc/gitlab/gitlab.rb
    echo "sidekiq['listen_address'] = '0.0.0.0'" >> /etc/gitlab/gitlab.rb
    echo "puma['listen'] = '0.0.0.0'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['monitoring_whitelist'] = ['$${private_subnet_cidr_env_var}', '127.0.0.0/8']" >> /etc/gitlab/gitlab.rb
    echo "nginx['status']['options']['allow'] = ['$${private_subnet_cidr_env_var}', '127.0.0.0/8']" >> /etc/gitlab/gitlab.rb

    ## Object Storage
    echo "gitlab_rails['object_store']['enabled'] = true" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['object_store']['connection'] = {" >> /etc/gitlab/gitlab.rb
    echo "  'provider' => 'AWS'," >> /etc/gitlab/gitlab.rb
    echo "  'region' => '$home_region'," >> /etc/gitlab/gitlab.rb
    echo "  'aws_access_key_id' => '$access_key'," >> /etc/gitlab/gitlab.rb
    echo "  'aws_secret_access_key' => '$secret_key'," >> /etc/gitlab/gitlab.rb
    echo "  'host' => '$namespace.compat.objectstorage.$home_region.oraclecloud.com'," >> /etc/gitlab/gitlab.rb
    echo "  'path_style' => true," >> /etc/gitlab/gitlab.rb
    echo "  'enable_signature_v4_streaming' => false" >> /etc/gitlab/gitlab.rb
    echo "}" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['object_store']['objects']['artifacts']['bucket'] = '$artifacts'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['object_store']['objects']['external_diffs']['bucket'] = '$external_diffs'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['object_store']['objects']['lfs']['bucket'] = '$lfs'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['object_store']['objects']['uploads']['bucket'] = '$uploads'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['object_store']['objects']['packages']['bucket'] = '$packages'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['object_store']['objects']['dependency_proxy']['bucket'] = '$dependency_proxy'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['object_store']['objects']['terraform_state']['bucket'] = '$terraform_state'" >> /etc/gitlab/gitlab.rb

    ## Backup setting to object storage
    echo "gitlab_rails['manage_backup_path'] = true" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['backup_path'] = '/var/opt/gitlab/backups'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['backup_archive_permissions'] = 0644" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['backup_pg_schema'] = 'public'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['backup_keep_time'] = 604800" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['backup_upload_connection'] = {" >> /etc/gitlab/gitlab.rb
    echo "  'provider' => 'AWS'," >> /etc/gitlab/gitlab.rb
    echo "  'region' => '$home_region'," >> /etc/gitlab/gitlab.rb
    echo "  'aws_access_key_id' => '$access_key'," >> /etc/gitlab/gitlab.rb
    echo "  'aws_secret_access_key' => '$secret_key'," >> /etc/gitlab/gitlab.rb
    echo "  'host' => '$namespace.compat.objectstorage.$home_region.oraclecloud.com'," >> /etc/gitlab/gitlab.rb
    echo "  'path_style' => true," >> /etc/gitlab/gitlab.rb
    echo "  'enable_signature_v4_streaming' => false" >> /etc/gitlab/gitlab.rb
    echo "}" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['backup_upload_remote_directory'] = '$backup_upload_remote_directory'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['backup_multipart_chunk_size'] = 104857600" >> /etc/gitlab/gitlab.rb

    ## Email SMTP settings to object
    echo "gitlab_rails['smtp_enable'] = true" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['smtp_address'] = 'smtp.email.$home_region.oci.oraclecloud.com'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['smtp_port'] = 587" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['smtp_user_name'] = '$smtp_user'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['smtp_password'] = '$smtp_passwd'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['smtp_domain'] = '$smtp_domain'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['smtp_authentication'] = 'plain'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['smtp_enable_starttls_auto'] = true" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['smtp_openssl_verify_mode'] = 'peer'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['gitlab_email_from'] = 'gitlab@`hostname`.$smtp_domain'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['gitlab_email_reply_to'] = 'no-reply@`hostname`.$smtp_domain'" >> /etc/gitlab/gitlab.rb
 
    ##########################################################
    ## Install 'oci cli' so we can upload gitlab-secrets.json 
    ## and share across nodes.
    ##########################################################
    yum install -y python36-oci-cli
 
    if [[ `hostname` == *0 ]]; then
        gitlab-ctl reconfigure
        if [ -f /etc/gitlab/gitlab-secrets.json ]; then
            oci os object put -bn $bucket --name gitlab-secrets.json.gitlab-server2 \
                              --file /etc/gitlab/gitlab-secrets.json --verify-checksum \
                              --force --auth instance_principal
            oci os object put -bn $bucket --name gitlab-secrets.json.$gitaly_server1_ip_env_var \
                              --file /etc/gitlab/gitlab-secrets.json --verify-checksum --force \
                              --auth instance_principal
            oci os object put -bn $bucket --name gitlab-secrets.json.$gitaly_server2_ip_env_var \
                              --file /etc/gitlab/gitlab-secrets.json --verify-checksum --force \
                              --auth instance_principal
        else
            echo "ERROR /etc/gitlab/gitlab-secrets.json does not exist after running gitlab-ctl reconfigure!"
            exit 1
        fi
        gitlab-rake gitlab:db:configure
        
        ## Add a cron job to run everyday at 00:00
        (crontab -l ; echo "00 00 * * * gitlab-rake gitlab:backup:create") | crontab -
    fi
 
    if [[ `hostname` == *1 ]]; then       
        while :
            do  
                echo "Attempting to get gitlab-secrets.json file..."
                oci os object get -bn $bucket --name gitlab-secrets.json.gitlab-server2 \
                                  --file gitlab-secrets.json --auth instance_principal
            if [ $? -eq 0 ]; then
                echo "Success, gitlab-secrets.json downloaded"
                oci os object delete -bn $bucket --name gitlab-secrets.json.gitlab-server2 \
                                     --force --auth instance_principal
                gitlab-ctl reconfigure
                mv /etc/gitlab/gitlab-secrets.json /etc/gitlab/gitlab-secrets.json.bkp
                mv gitlab-secrets.json /etc/gitlab/gitlab-secrets.json
                gitlab-ctl reconfigure
                break
            fi  
            sleep 10s 
            done
        ## Add a cron job to run everyday at 12:00
        (crontab -l ; echo "00 12 * * * gitlab-rake gitlab:backup:create") | crontab -
    fi  
}

setup_postgres_config() {

    firewall-offline-cmd --zone=public  --add-port=5432/tcp
    firewall-offline-cmd --zone=public  --add-port=9100/tcp
    firewall-offline-cmd --zone=public  --add-port=9187/tcp
    systemctl restart firewalld.service

    sed -i"" "s#external_url 'http://gitlab.example.com'##g" /etc/gitlab/gitlab.rb
    echo "roles ['postgres_role']" >> /etc/gitlab/gitlab.rb
    echo "patroni['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "consul['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "prometheus['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "alertmanager['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "pgbouncer_exporter['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "redis_exporter['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "gitlab_exporter['enable'] = false" >> /etc/gitlab/gitlab.rb

    echo "node_exporter['listen_address'] = '0.0.0.0:9100'" >> /etc/gitlab/gitlab.rb
    echo "postgres_exporter['listen_address'] = '0.0.0.0:9187'" >> /etc/gitlab/gitlab.rb
    echo "postgres_exporter['dbname'] = 'gitlabhq_production'" >> /etc/gitlab/gitlab.rb
    echo "postgres_exporter['password'] = '$POSTGRESQL_PASSWD'" >> /etc/gitlab/gitlab.rb
 
    echo "postgresql['listen_address'] = '0.0.0.0'" >> /etc/gitlab/gitlab.rb
    echo "postgresql['port'] = 5432" >> /etc/gitlab/gitlab.rb
    echo "postgresql['sql_user_password'] = '$POSTGRESQL_PASSWD'" >> /etc/gitlab/gitlab.rb
    echo "postgresql['trust_auth_cidr_addresses'] = %w(127.0.0.1/32 $subnet_cidr_block_env_var)" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['auto_migrate'] = false" >> /etc/gitlab/gitlab.rb

    gitlab-ctl reconfigure
}

setup_redis_config() {

    firewall-offline-cmd --zone=public  --add-port=6379/tcp
    firewall-offline-cmd --zone=public  --add-port=9100/tcp
    firewall-offline-cmd --zone=public  --add-port=9121/tcp
    systemctl restart firewalld.service
 
    sed -i"" "s#external_url 'http://gitlab.example.com'##g" /etc/gitlab/gitlab.rb
    echo "redis['enable'] = true" >> /etc/gitlab/gitlab.rb
    echo "sidekiq['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "gitlab_workhorse['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "puma['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "unicorn['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "postgresql['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "nginx['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "prometheus['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "alertmanager['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "pgbouncer_exporter['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "gitlab_exporter['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "gitaly['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "grafana['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "redis['bind'] = '0.0.0.0'" >> /etc/gitlab/gitlab.rb
    echo "redis['port'] = 6379" >> /etc/gitlab/gitlab.rb
    echo "redis['password'] = '$initial_passwd'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "node_exporter['listen_address'] = '0.0.0.0:9100'" >> /etc/gitlab/gitlab.rb
    echo "redis_exporter['listen_address'] = '0.0.0.0:9121'" >> /etc/gitlab/gitlab.rb
    echo "redis_exporter['flags'] = {" >> /etc/gitlab/gitlab.rb
    echo "      'redis.addr' => 'redis://0.0.0.0:6379'," >> /etc/gitlab/gitlab.rb
    echo "      'redis.password' => '$initial_passwd'," >> /etc/gitlab/gitlab.rb
    echo "}" >> /etc/gitlab/gitlab.rb

    gitlab-ctl reconfigure
}


setup_gitaly_config() {

    firewall-offline-cmd --zone=public  --add-port=8075/tcp
    firewall-offline-cmd --zone=public  --add-port=9236/tcp
    firewall-offline-cmd --zone=public  --add-port=9100/tcp
    systemctl restart firewalld.service

    /usr/libexec/oci-growfs -y
    sed -i"" "s#external_url 'http://gitlab.example.com'##g" /etc/gitlab/gitlab.rb
    echo "gitaly['auth_token'] = '$initial_passwd'" >> /etc/gitlab/gitlab.rb
    echo "gitlab_shell['secret_token'] = '$initial_passwd'" >> /etc/gitlab/gitlab.rb
    echo "postgresql['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "redis['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "nginx['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "puma['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "unicorn['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "sidekiq['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "gitlab_workhorse['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "grafana['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "alertmanager['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "prometheus['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['rake_cache_clear'] = false" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['auto_migrate'] = false" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['internal_api_url'] = 'http://$load_balancer_ip_env_var'" >> /etc/gitlab/gitlab.rb
    echo "gitaly['listen_addr'] = '0.0.0.0:8075'" >> /etc/gitlab/gitlab.rb
    echo "gitaly['prometheus_listen_addr'] = '0.0.0.0:9236'" >> /etc/gitlab/gitlab.rb
    echo "node_exporter['listen_address'] = '0.0.0.0:9100'" >> /etc/gitlab/gitlab.rb
    echo "git_data_dirs({" >> /etc/gitlab/gitlab.rb
    echo "  'default' => {" >> /etc/gitlab/gitlab.rb
    echo "    'path' => '/var/opt/gitlab/git-data'" >> /etc/gitlab/gitlab.rb
    echo "  }" >> /etc/gitlab/gitlab.rb
    echo "})" >> /etc/gitlab/gitlab.rb

    gitlab-ctl reconfigure

    yum install -y python36-oci-cli
    ip_ad=`hostname -I`
    while :
        do  
            echo "Attempting to get config..."
            oci os object get \
              -bn $bucket --name gitlab-secrets.json.$${ip_ad} --file gitlab-secrets.json --auth instance_principal
            if [ $? -eq 0 ]; then
                echo "Success, gitlab-secrets.json downloaded"
                oci os object delete -bn $bucket --name gitlab-secrets.json.$${ip_ad} --force --auth instance_principal
                mv /etc/gitlab/gitlab-secrets.json /etc/gitlab/gitlab-secrets.json.bkp
                mv gitlab-secrets.json /etc/gitlab/gitlab-secrets.json
                break
            fi  
            sleep 10s 
        done

    gitlab-ctl reconfigure
}


setup_monitoring_config() {

    firewall-offline-cmd --zone=public  --add-port=80/tcp
    firewall-offline-cmd --zone=public  --add-port=443/tcp
    firewall-offline-cmd --zone=public  --add-port=3000/tcp
    firewall-offline-cmd --zone=public  --add-port=9090/tcp
    firewall-offline-cmd --zone=public  --add-port=9187/tcp
    firewall-offline-cmd --zone=public  --add-port=9121/tcp
    firewall-offline-cmd --zone=public  --add-port=9236/tcp
    firewall-offline-cmd --zone=public  --add-port=8060/tcp
    firewall-offline-cmd --zone=public  --add-port=9229/tcp
    firewall-offline-cmd --zone=public  --add-port=8080/tcp
    firewall-offline-cmd --zone=public  --add-port=8082/tcp
    firewall-offline-cmd --zone=public  --add-port=9100/tcp
    systemctl restart firewalld.service

    sed -i"" "s#external_url 'http://gitlab.example.com'##g" /etc/gitlab/gitlab.rb
    echo "external_url 'https://$load_balancer_ip_env_var'" >> /etc/gitlab/gitlab.rb
    echo "letsencrypt['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "prometheus['enable'] = true" >> /etc/gitlab/gitlab.rb
    echo "prometheus['listen_address'] = '0.0.0.0:9090'" >> /etc/gitlab/gitlab.rb
    echo "prometheus['monitor_kubernetes'] = false" >> /etc/gitlab/gitlab.rb
    echo "grafana['disable_login_form'] = false" >> /etc/gitlab/gitlab.rb
    echo "grafana['enable'] = true" >> /etc/gitlab/gitlab.rb
    echo "grafana['admin_password'] = '$initial_passwd'" >> /etc/gitlab/gitlab.rb
    echo "grafana['http_addr'] = '0.0.0.0'" >> /etc/gitlab/gitlab.rb
    echo "grafana['http_port'] = 3000" >> /etc/gitlab/gitlab.rb
    echo "gitlab_rails['auto_migrate'] = false" >> /etc/gitlab/gitlab.rb
    echo "alertmanager['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "gitaly['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "gitlab_exporter['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "gitlab_workhorse['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "nginx['enable'] = true" >> /etc/gitlab/gitlab.rb
    echo "postgres_exporter['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "postgresql['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "redis['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "redis_exporter['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "sidekiq['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "puma['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "unicorn['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "node_exporter['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "gitlab_exporter['enable'] = false" >> /etc/gitlab/gitlab.rb
    echo "prometheus['scrape_configs'] = [" >> /etc/gitlab/gitlab.rb
    echo "  {" >> /etc/gitlab/gitlab.rb
    echo "     'job_name': 'postgres'," >> /etc/gitlab/gitlab.rb
    echo "     'static_configs' => [" >> /etc/gitlab/gitlab.rb
    echo "     'targets' => ['$${postgres_server_ip_env_var}:9187']," >> /etc/gitlab/gitlab.rb
    echo "     ]," >> /etc/gitlab/gitlab.rb
    echo "  }," >> /etc/gitlab/gitlab.rb
    echo "  {" >> /etc/gitlab/gitlab.rb
    echo "     'job_name': 'redis'," >> /etc/gitlab/gitlab.rb
    echo "     'static_configs' => [" >> /etc/gitlab/gitlab.rb
    echo "     'targets' => ['$${redis_server_ip_env_var}:9121']," >> /etc/gitlab/gitlab.rb
    echo "     ]," >> /etc/gitlab/gitlab.rb
    echo "  }," >> /etc/gitlab/gitlab.rb
    echo "  {" >> /etc/gitlab/gitlab.rb
    echo "     'job_name': 'gitaly'," >> /etc/gitlab/gitlab.rb
    echo "     'static_configs' => [" >> /etc/gitlab/gitlab.rb
    echo "     'targets' => ['$${gitaly_server1_ip_env_var}:9236', '$${gitaly_server2_ip_env_var}:9236']," >> /etc/gitlab/gitlab.rb
    echo "     ]," >> /etc/gitlab/gitlab.rb
    echo "  }," >> /etc/gitlab/gitlab.rb
    echo "  {" >> /etc/gitlab/gitlab.rb
    echo "     'job_name': 'gitlab-nginx'," >> /etc/gitlab/gitlab.rb
    echo "     'static_configs' => [" >> /etc/gitlab/gitlab.rb
    echo "     'targets' => ['$${gitlab_server1_ip_env_var}:8060', '$${gitlab_server2_ip_env_var}:8060']," >> /etc/gitlab/gitlab.rb
    echo "     ]," >> /etc/gitlab/gitlab.rb
    echo "  }," >> /etc/gitlab/gitlab.rb
    echo "  {" >> /etc/gitlab/gitlab.rb
    echo "     'job_name': 'gitlab-workhorse'," >> /etc/gitlab/gitlab.rb
    echo "     'static_configs' => [" >> /etc/gitlab/gitlab.rb
    echo "     'targets' => ['$${gitlab_server1_ip_env_var}:9229', '$${gitlab_server2_ip_env_var}:9229']," >> /etc/gitlab/gitlab.rb
    echo "     ]," >> /etc/gitlab/gitlab.rb
    echo "  }," >> /etc/gitlab/gitlab.rb
    echo "  {" >> /etc/gitlab/gitlab.rb
    echo "     'job_name': 'gitlab-rails'," >> /etc/gitlab/gitlab.rb
    echo "     'metrics_path': '/-/metrics'," >> /etc/gitlab/gitlab.rb
    echo "     'static_configs' => [" >> /etc/gitlab/gitlab.rb
    echo "     'targets' => ['$${gitlab_server1_ip_env_var}:8080', '$${gitlab_server2_ip_env_var}:8080']," >> /etc/gitlab/gitlab.rb
    echo "     ]," >> /etc/gitlab/gitlab.rb
    echo "  }," >> /etc/gitlab/gitlab.rb
    echo "  {" >> /etc/gitlab/gitlab.rb
    echo "     'job_name': 'gitlab-sidekiq'," >> /etc/gitlab/gitlab.rb
    echo "     'static_configs' => [" >> /etc/gitlab/gitlab.rb
    echo "     'targets' => ['$${gitlab_server1_ip_env_var}:8082', '$${gitlab_server2_ip_env_var}:8082']," >> /etc/gitlab/gitlab.rb
    echo "     ]," >> /etc/gitlab/gitlab.rb
    echo "  }," >> /etc/gitlab/gitlab.rb
    echo "  {" >> /etc/gitlab/gitlab.rb
    echo "     'job_name': 'node'," >> /etc/gitlab/gitlab.rb
    echo "     'static_configs' => [" >> /etc/gitlab/gitlab.rb
    echo "     'targets' => ['$${postgres_server_ip_env_var}:9100', '$${redis_server_ip_env_var}:9100', '$${gitaly_server1_ip_env_var}:9100', '$${gitaly_server2_ip_env_var}:9100', '$${gitlab_server1_ip_env_var}:9100','$${gitlab_server2_ip_env_var}:9100']," >> /etc/gitlab/gitlab.rb
    echo "     ]," >> /etc/gitlab/gitlab.rb
    echo "  }," >> /etc/gitlab/gitlab.rb
    echo "]" >> /etc/gitlab/gitlab.rb

    gitlab-ctl reconfigure

}

install_gitlab
export POSTGRESQL_PASSWD=`gitlab-ctl pg-password-md5 gitlab <<< $${initial_passwd}`
if [ "$IS_GITLAB_SERVER" == 1 ]; then 
  setup_gitlab_config
elif [ "$IS_POSTGRES_SERVER" == 1 ]; then
  setup_postgres_config
elif [ "$IS_REDIS_SERVER" == 1 ]; then
  setup_redis_config
elif [ "$IS_GITALY_SERVER" == 1 ]; then
  setup_gitaly_config
elif [ "$IS_MONITORING_SERVER" == 1 ]; then
  setup_monitoring_config
fi
