resource "oci_core_instance" "postgres-server" {
  availability_domain = local.availability_domain
  compartment_id      = var.compute_compartment_ocid
  display_name        = "postgres-server"
  shape               = var.postgres_server_shape

  dynamic "shape_config" {
    for_each = local.is_flex_shape_postgres
    content {
      ocpus = var.postgres_server_ocpus
      memory_in_gbs = var.postgres_server_ram
    }
  }

  create_vnic_details {
    subnet_id              = local.private_subnet_id
    display_name           = "postgres-server"
    assign_public_ip       = false
    hostname_label         = "postgres-server"
    skip_source_dest_check = false
    nsg_ids                = [oci_core_network_security_group.simple_nsg.id]
  }

  source_details {
    source_type = "image"
    source_id   = local.platform_image_id
  }

  metadata = {
    ssh_authorized_keys = var.ssh_public_key
    user_data = base64encode(join("\n", list(
      "#!/usr/bin/env bash",
      "set -x",
      "export IS_POSTGRES_SERVER=1",
      "export subnet_cidr_block_env_var=\"${data.oci_core_subnet.gitlab_private_subnet.cidr_block}\"",
      "export initial_passwd=\"${random_password.password.result}\"",
      data.template_file.setup_gitlab-OL.rendered,
    )))
  }

  freeform_tags = map(var.tag_key_name, var.tag_value)
}
